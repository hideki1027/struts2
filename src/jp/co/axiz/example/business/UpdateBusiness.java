package jp.co.axiz.example.business;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.axiz.example.db.dao.LogintableDAO;
import jp.co.axiz.example.db.dao.UsertableDAO;
import jp.co.axiz.example.db.entity.Logintable;
import jp.co.axiz.example.db.entity.Usertable;
import jp.co.axiz.example.db.entity.base.BaseEntity;
import jp.co.axiz.util.DbUtil;

public class UpdateBusiness {
	public int doUpdateUserInfo(ArrayList<BaseEntity> aryEntity, int userid) throws Exception {

		// コネクションを定義
		Connection con = null;

		// 返却値
		int numRet = 0;

		try {
			// コネクションを確立
			con = DbUtil.getConnection();

			// メソッド内使用インスタンスの生成
			UsertableDAO usertableDAO = new UsertableDAO(con);
			LogintableDAO logintableDAO = new LogintableDAO(con);

			// 引数の取得
			Usertable usertable = (Usertable)aryEntity.get(0);
			Logintable logintable = (Logintable)aryEntity.get(1);

			// 登録値の格納
			ArrayList<Object> aryParam = new ArrayList<Object>();
			aryParam.add(usertable.getUsername());
			aryParam.add(usertable.getTel());

			// 登録の実行
			numRet = usertableDAO.updateByPrimaryKey(aryParam, userid);

			ArrayList<Object> aryParamLogin = new ArrayList<Object>();
			aryParamLogin.add(logintable.getPass());

			// ログインテーブルの登録
			numRet = logintableDAO.updateByPrimaryKey(aryParamLogin, userid);

			// トランザクションの確定
			con.commit();
		} catch(SQLException e) {
			System.out.println(e);
			con.rollback();
		} finally {
			// コネクションをクローズ
			DbUtil.closeConnection(con);
		}

		// 結果を返却
		return numRet;
	}
}
