package jp.co.axiz.example.business;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.axiz.example.db.dao.LogintableDAO;
import jp.co.axiz.example.db.dao.UsertableDAO;
import jp.co.axiz.example.db.entity.Logintable;
import jp.co.axiz.example.db.entity.Usertable;
import jp.co.axiz.example.db.entity.base.BaseEntity;
import jp.co.axiz.util.DbUtil;

public class SelectBusiness {
	public Usertable doLogin(int userid, String pass) throws Exception {
		// コネクションを定義
		Connection con = null;

		// 返却値
		Usertable usertable = null;

		try {
			// コネクションを確立
			con = DbUtil.getConnection();

			// メソッド内使用インスタンスの生成
			UsertableDAO usertableDAO = new UsertableDAO(con);

			// ログイン用検索処理を行う
			ArrayList<BaseEntity> aryResult = usertableDAO.selectWithLoginTable(userid, pass);


			// 置き換え処理を行う
			if (aryResult != null && aryResult.size() > 0) {
				usertable = (Usertable)aryResult.get(0);
			}
		} catch(SQLException e) {
			System.out.println(e);
		} finally {
			// コネクションをクローズ
			DbUtil.closeConnection(con);
		}

		// 結果の返却
		return usertable;
	}

	/**
	 * 検索トランザクション処理を行う
	 * @throws Exception
	 */
	public ArrayList<Usertable> doSelectByKey(ArrayList<Object> aryParam) throws Exception {

		// コネクションを定義
		Connection con = null;

		// メソッド内使用変数を定義
		ArrayList<BaseEntity> arySelect = null;
		ArrayList<Usertable> aryRet = null;

		try {
			// コネクションを確立
			con = DbUtil.getConnection();

			// メソッド内使用インスタンスの生成
			UsertableDAO usertableDAO = new UsertableDAO(con);

			// 全検索処理を行う
			arySelect = usertableDAO.findAll();
		} catch(SQLException e) {
			System.out.println(e);
		} finally {
			// コネクションをクローズ
			DbUtil.closeConnection(con);
		}

		// 結果判定
		if (arySelect != null && arySelect.size() > 0) {
			// 結果を表示する処理
			aryRet = new ArrayList<Usertable>();
			for (BaseEntity baseEntity : arySelect) {
				// Usertableエンティティへキャスト
				Usertable usertable = (Usertable)baseEntity;
				aryRet.add(usertable);
			}
		}

		// 結果を返却
		return aryRet;
	}
	/**
	 * ユーザIDからユーザ情報を取得する
	 * @param userid
	 * @return
	 * @throws Exception
	 */
	public ArrayList<BaseEntity> doSelectUserInfo(int userid) throws Exception {
		// コネクションを定義
		Connection con = null;

		// 返却値
		ArrayList<BaseEntity> aryResult = new ArrayList<BaseEntity>();

		try {
			// コネクションを確立
			con = DbUtil.getConnection();

			// メソッド内使用インスタンスの生成
			UsertableDAO usertableDAO = new UsertableDAO(con);
			LogintableDAO logintableDAO = new LogintableDAO(con);

			// 検索女権
			ArrayList<Object> aryParam = new ArrayList<Object>();
			aryParam.add(userid);

			// ログイン用検索処理を行う
			ArrayList<BaseEntity> aryRetUser = usertableDAO.findByPrimaryKey(aryParam);

			// 結果の判定
			if (aryRetUser != null && aryRetUser.size() > 0) {
				aryResult.add((Usertable)aryRetUser.get(0));

				// ログインテーブルの検索
				aryRetUser = logintableDAO.findByPrimaryKey(aryParam);
				if (aryRetUser != null && aryRetUser.size() > 0) {
					aryResult.add((Logintable)aryRetUser.get(0));
				}
			}
		} catch(SQLException e) {
			System.out.println(e);
		} finally {
			// コネクションをクローズ
			DbUtil.closeConnection(con);
		}

		// 結果の返却
		return aryResult;
	}

	/**
	 * ログインを行う
	 * @param aryParam
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Usertable> doLogin(ArrayList<Object> aryParam) throws Exception {
		// コネクションを定義
		Connection con = null;

		// 返却値
		ArrayList<Usertable> aryUserInfo = new ArrayList<Usertable>();

		try {
			// コネクションを確立
			con = DbUtil.getConnection();

			// メソッド内使用インスタンスの生成
			UsertableDAO usertableDAO = new UsertableDAO(con);

			// ログイン用検索処理を行う
			ArrayList<BaseEntity> aryResult = usertableDAO.selectByLikeKey(aryParam);


			// 置き換え処理を行う
			if (aryResult != null && aryResult.size() > 0) {
				for (int i = 0; i < aryResult.size(); i++) {
					aryUserInfo.add((Usertable)aryResult.get(i));
				}
			}
		} catch(SQLException e) {
			System.out.println(e);
		} finally {
			// コネクションをクローズ
			DbUtil.closeConnection(con);
		}

		// 結果の返却
		return aryUserInfo;
	}

}
