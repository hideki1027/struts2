package jp.co.axiz.example.business;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.axiz.example.db.dao.LogintableDAO;
import jp.co.axiz.example.db.dao.UsertableDAO;
import jp.co.axiz.example.db.entity.Logintable;
import jp.co.axiz.example.db.entity.Usertable;
import jp.co.axiz.example.db.entity.base.BaseEntity;
import jp.co.axiz.util.DbUtil;

public class InsertBusiness {
	/**
	 * 登録トランザクション処理を行う
	 * @param aryEntity
	 * @return 登録ユーザID
	 * @throws Exception
	 */
	public int doInsertUserInfo(ArrayList<BaseEntity> aryEntity) throws Exception {

		// コネクションを定義
		Connection con = null;

		// 返却値
		int numRet = 0;

		try {
			// コネクションを確立
			con = DbUtil.getConnection();

			// メソッド内使用インスタンスの生成
			UsertableDAO usertableDAO = new UsertableDAO(con);
			LogintableDAO logintableDAO = new LogintableDAO(con);

			// 引数の取得
			Usertable usertable = (Usertable)aryEntity.get(0);
			Logintable logintable = (Logintable)aryEntity.get(1);

			// 登録値の格納
			ArrayList<Object> aryParam = new ArrayList<Object>();
			aryParam.add(usertable.getUsername());
			aryParam.add(usertable.getTel());

			// 登録の実行
			usertableDAO.insertByAllColumns(aryParam);

			// 登録後に、現在最大のID番号を取得
			Integer maxId = usertableDAO.selectMaxId();
			ArrayList<Object> aryParamLogin = new ArrayList<Object>();
			aryParamLogin.add(maxId);
			aryParamLogin.add(logintable.getPass());

			// ログインテーブルの登録
			logintableDAO.insertByAllColumns(aryParamLogin);

			// トランザクションの確定
			con.commit();
			numRet = maxId;

		} catch(SQLException e) {
			System.out.println(e);
			con.rollback();
		} finally {
			// コネクションをクローズ
			DbUtil.closeConnection(con);
		}

		// 結果を返却
		return numRet;
	}
}
