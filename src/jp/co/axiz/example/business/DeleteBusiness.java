package jp.co.axiz.example.business;

import java.sql.Connection;
import java.sql.SQLException;

import jp.co.axiz.example.db.dao.LogintableDAO;
import jp.co.axiz.example.db.dao.UsertableDAO;
import jp.co.axiz.util.DbUtil;

public class DeleteBusiness {
	public int doDeleteUserInfo(int userid) throws Exception {

		// コネクションを定義
		Connection con = null;

		// 返却値
		int numRet = 0;

		try {
			// コネクションを確立
			con = DbUtil.getConnection();

			// メソッド内使用インスタンスの生成
			UsertableDAO usertableDAO = new UsertableDAO(con);
			LogintableDAO logintableDAO = new LogintableDAO(con);

			// 削除の実行
			numRet = usertableDAO.deleteByPrimaryKey(userid);

			// ログインテーブルの登録
			numRet = logintableDAO.deleteByPrimaryKey(userid);

			// トランザクションの確定
			con.commit();
		} catch(SQLException e) {
			System.out.println(e);
			con.rollback();
		} finally {
			// コネクションをクローズ
			DbUtil.closeConnection(con);
		}

		// 結果を返却
		return numRet;
	}

}
