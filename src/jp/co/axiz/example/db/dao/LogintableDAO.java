package jp.co.axiz.example.db.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.axiz.example.db.dao.base.BaseDao;
import jp.co.axiz.example.db.entity.Logintable;
import jp.co.axiz.example.db.entity.base.BaseEntity;

public class LogintableDAO extends BaseDao {
	private String INSERT_ALL_COLUMNS = "INSERT INTO " + this.tableId + " (loginid, pass) VALUES (?, ?)";
	private String UPDATE_BY_PK = "UPDATE " + this.tableId + " SET pass = ? WHERE loginid = ?";
	private String DELETE_BY_PK = "DELETE FROM " + this.tableId + " WHERE loginid = ?";

	/**
	 * コンストラクタ
	 * @param con
	 */
	public LogintableDAO(Connection con) {
		super(con);
	}

	@Override
	protected String getTableId() {
		// TODO 自動生成されたメソッド・スタブ
		return "logintable";
	}

	@Override
	protected BaseEntity rowMapping(ResultSet rs) throws SQLException {
		Logintable logintable = new Logintable();
		logintable.setLoginid(rs.getInt("loginid"));
		logintable.setPass(rs.getString("pass"));
		return logintable;
	}

	@Override
	protected ArrayList<String> setPrimaryKey() {
		ArrayList<String> aryPK = new ArrayList<String>();
		aryPK.add("loginid");
		return aryPK;
	}

	//============================================================== DAO個別メソッド

	/**
	 * 登録を行うメソッド
	 * @param aryParam 登録値 String username, String telの順に指定（必須）
	 * @return 登録件数
	 */
	public int insertByAllColumns(ArrayList<Object> aryParam) throws SQLException {
		// SQLの指定
		this.stmt = con.prepareStatement(INSERT_ALL_COLUMNS);

		// パラメータ値のセット
		super.setParamValue(aryParam);

		// SQLの実行結果を返却
		return this.doExecuteUpdate();
	}

	/**
	 * 主キーを更新条件に値を登録するメソッド
	 * @param aryParam 更新値 String username, String telの順に指定（必須）
	 * @return
	 */
	public int updateByPrimaryKey(ArrayList<Object> aryParam, int userid) throws SQLException {
		// SQLの指定
		this.stmt = con.prepareStatement(UPDATE_BY_PK);

		// パラメータ値のセット
		super.setParamValue(aryParam);
		// 主キーを最後にセット
		this.stmt.setInt(aryParam.size() + 1, userid);

		// SQLの実行結果を返却
		return this.doExecuteUpdate();
	}

	/**
	 * 主キーを削除条件にレコードを削除するメソッド
	 * @param aryParam
	 * @return
	 */
	public int deleteByPrimaryKey(int userid) throws SQLException {
		// SQLの指定
		this.stmt = con.prepareStatement(DELETE_BY_PK);

		// パラメータ値のセット
		this.stmt.setInt(1, userid);

		// SQLの実行結果を返却
		return this.doExecuteUpdate();
	}
}
