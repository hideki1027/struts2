package jp.co.axiz.example.db.dao.base;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.axiz.example.db.entity.base.BaseEntity;

/**
 * DAOの親クラス
 * @author AxiZ
 *
 */
public abstract class BaseDao {

	// フィールド定義
	protected Connection con;
	protected PreparedStatement stmt;
	protected String tableId;
	protected ArrayList<String> aryPrimaryKey;


	// コンストラクタ
	protected BaseDao(Connection con) {
		this.con = con;
		this.tableId = this.getTableId();
		this.aryPrimaryKey = this.setPrimaryKey();
	}

	/**
	 * 全件検索SQLの取得
	 * @return
	 */
	private String getSelectSQL() {
		return "SELECT * FROM " + this.tableId;
	}

	/**
	 *  絞り込み条件、登録／更新値等をSQLにセットする
	 * @param aryParam 条件
	 * @throws SQLException
	 */
	protected void setParamValue(ArrayList<Object> aryParam) throws SQLException {
		if (aryParam.size() > 0) {
			for (int i = 0; i < aryParam.size(); i++) {
				if (aryParam.get(i) instanceof Integer) {
					this.stmt.setInt(i + 1, Integer.parseInt(aryParam.get(i).toString()));
				} else if (aryParam.get(i) instanceof String) {
					this.stmt.setString(i + 1, aryParam.get(i).toString());
				}
			}
		}
	}

	/**
	 * 検索を行うSQLの実行
	 * @return 検索結果
	 * @throws SQLException
	 */
	protected ArrayList<BaseEntity> dpExecuteSelect() throws SQLException {
		// SQLの実行
		ResultSet rs = this.stmt.executeQuery();

		// マッピング
		ArrayList<BaseEntity> aryResult = new ArrayList<BaseEntity>();
		while (rs.next()) {
			aryResult.add(this.rowMapping(rs));
		}

		// stmtを閉じ、結果を返却
		stmt.close();
		return aryResult;
	}

	/**
	 * 更新・登録・削除を行うSQLの実行
	 * @return 変更件数
	 * @throws SQLException
	 */
	protected int doExecuteUpdate() throws SQLException {
		int ret = this.stmt.executeUpdate();
		this.stmt.close();
		return ret;
	}

	/**
	 *  全検索メソッド
	 * @return 検索結果
	 * @throws SQLException
	 */
	public ArrayList<BaseEntity> findAll() throws SQLException {
		// 全検索用のSQLを生成
		String sql = this.getSelectSQL();

		// ステートメントを生成
		this.stmt = con.prepareStatement(sql);

		// マッピング
		ArrayList<BaseEntity> aryResult = this.dpExecuteSelect();
		return aryResult;
	}

	/**
	 *  主キー検索メソッド
	 * @param aryParam 主キーの条件全ての条件値
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<BaseEntity> findByPrimaryKey(ArrayList<Object> aryParam) throws SQLException {
		// 全検索用のSQLを生成
		String sql = getSelectSQL();

		// Where句の生成
		if (this.aryPrimaryKey.size() > 0) {
			String strWhere = " WHERE ";
			for (int i = 0; i < this.aryPrimaryKey.size(); i++) {
				sql += strWhere + this.aryPrimaryKey.get(i).toString() + " = ?";
				strWhere = " AND ";
			}
		}

		// ステートメントを生成
		this.stmt = con.prepareStatement(sql);

		// 条件の適用
		this.setParamValue(aryParam);

		// マッピング
		ArrayList<BaseEntity> aryResult = this.dpExecuteSelect();
		return aryResult;
	}

	//============================================ 抽象メソッド
	/**
	 * テーブル物理名を取得する
	 * @return tableName
	 */
	protected abstract String getTableId();

	/**
	 * 取得したデータをマッピング
	 * @param rs
	 * @return ArrayList<BaseEntity>
	 */
	protected abstract BaseEntity rowMapping(ResultSet rs) throws SQLException;

	/**
	 * PKをセットする
	 * @return
	 */
	protected abstract ArrayList<String> setPrimaryKey();
}
