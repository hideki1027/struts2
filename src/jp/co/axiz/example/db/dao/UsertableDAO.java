package jp.co.axiz.example.db.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.axiz.example.db.dao.base.BaseDao;
import jp.co.axiz.example.db.entity.Usertable;
import jp.co.axiz.example.db.entity.base.BaseEntity;

/**
 * UsertableのDAOクラス
 * @author AxiZ
 *
 */
public class UsertableDAO extends BaseDao {
	private String INSERT_ALL_COLUMNS = "INSERT INTO " + this.tableId + " (username, tel) VALUES (?, ?)";
	private String UPDATE_BY_PK = "UPDATE " + this.tableId + " SET username = ?, tel = ? WHERE userid = ?";
	private String DELETE_BY_PK = "DELETE FROM " + this.tableId + " WHERE userid = ?";

	/**
	 * コンストラクタ
	 * @param con
	 */
	public UsertableDAO(Connection con) {
		super(con);
	}

	@Override
	protected String getTableId() {
		return "usertable";
	}

	@Override
	protected BaseEntity rowMapping(ResultSet rs) throws SQLException {
		Usertable usertable = new Usertable();
		usertable.setUserid(rs.getInt("userid"));
		usertable.setUsername(rs.getString("username"));
		usertable.setTel(rs.getString("tel"));
		return usertable;
	}

	@Override
	protected ArrayList<String> setPrimaryKey() {
		ArrayList<String> aryPK = new ArrayList<String>();
		aryPK.add("userid");
		return aryPK;
	}

	//============================================================== DAO個別メソッド

	/**
	 * 登録を行うメソッド
	 * @param aryParam 登録値 String username, String telの順に指定（必須）
	 * @return 登録件数
	 */
	public int insertByAllColumns(ArrayList<Object> aryParam) throws SQLException {
		// SQLの指定
		this.stmt = con.prepareStatement(INSERT_ALL_COLUMNS);

		// パラメータ値のセット
		super.setParamValue(aryParam);

		// SQLの実行結果を返却
		return this.doExecuteUpdate();
	}

	/**
	 * 主キーを更新条件に値を登録するメソッド
	 * @param aryParam 更新値 String username, String telの順に指定（必須）
	 * @return
	 */
	public int updateByPrimaryKey(ArrayList<Object> aryParam, int userid) throws SQLException {
		// SQLの指定
		this.stmt = con.prepareStatement(UPDATE_BY_PK);

		// パラメータ値のセット
		super.setParamValue(aryParam);
		// 主キーを最後にセット
		this.stmt.setInt(aryParam.size() + 1, userid);

		// SQLの実行結果を返却
		return this.doExecuteUpdate();
	}

	/**
	 * 主キーを削除条件にレコードを削除するメソッド
	 * @param aryParam
	 * @return
	 */
	public int deleteByPrimaryKey(int userid) throws SQLException {
		// SQLの指定
		this.stmt = con.prepareStatement(DELETE_BY_PK);

		// パラメータ値のセット
		this.stmt.setInt(1, userid);

		// SQLの実行結果を返却
		return this.doExecuteUpdate();
	}

	public ArrayList<BaseEntity> selectByLikeKey(ArrayList<Object> aryParam) throws SQLException {
		// SQLの生成
		StringBuffer strSQL = new StringBuffer();
		strSQL.append("SELECT");
		strSQL.append("   userid");
		strSQL.append("  ,username");
		strSQL.append("  ,tel");
		strSQL.append(" FROM");
		strSQL.append("   usertable");

		// 検索条件
		ArrayList<Object> aryKey = new ArrayList<Object>();

		// 絞り込み条件の生成
		String strWhere = " WHERE ";
		if(!"".equals(aryParam.get(0).toString().trim())) {
			strSQL.append(strWhere + "userid = ?");
			strWhere = " OR ";
			aryKey.add(Integer.parseInt(aryParam.get(0).toString()));
		}
		if(!"".equals(aryParam.get(1).toString().trim())) {
			strSQL.append(strWhere + "username LIKE ?");
			strWhere = " OR ";
			aryKey.add("%" + aryParam.get(1) + "%");
		}
		if(!"".equals(aryParam.get(2).toString().trim())) {
			strSQL.append(strWhere + "tel LIKE ?");
			strWhere = " OR ";
			aryKey.add("%" + aryParam.get(2) + "%");
		}

		// SQLの指定
		this.stmt = con.prepareStatement(strSQL.toString());

		// パラメータをセット
		super.setParamValue(aryKey);

		// 検索の実行
		return dpExecuteSelect();
	}

	/**
	 * ログイン用
	 * @param userid
	 * @param pass
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<BaseEntity> selectWithLoginTable(int userid, String pass) throws SQLException {
		// SQLの生成
		StringBuffer strSQL = new StringBuffer();
		strSQL.append("SELECT");
		strSQL.append("   UT.userid");
		strSQL.append("  ,UT.username");
		strSQL.append("  ,UT.tel");
		strSQL.append(" FROM");
		strSQL.append("   usertable UT");
		strSQL.append("  ,logintable LT");
		strSQL.append(" WHERE");
		strSQL.append("   UT.userid = LT.loginid");
		strSQL.append("   AND UT.userid = ?");
		strSQL.append("   AND LT.pass = ?");

		// SQLの指定
		this.stmt = con.prepareStatement(strSQL.toString());

		// パラメータ値のセット
		this.stmt.setInt(1, userid);
		this.stmt.setString(2, pass);

		// SQLの実行
		return this.dpExecuteSelect();
	}

	/**
	 * IDの最大値を取得
	 * @return
	 * @throws SQLException
	 */
	public int selectMaxId() throws SQLException {
		// 返却値の定義
		int numRet = 0;

		// SQLの生成
		StringBuffer strSQL = new StringBuffer();
		strSQL.append("SELECT MAX(userid) AS max FROM usertable");

		// SQLの指定
		this.stmt = con.prepareStatement(strSQL.toString());

		// SQLの実行
		ResultSet rs = this.stmt.executeQuery();

		// 結果の取得
		if (rs.next()) {
			// 必ず一件取得できる
			numRet = rs.getInt("max");
		}
		this.stmt.close();

		// 結果の返却
		return numRet;
	}
}
