package jp.co.axiz.example.db.entity;

import jp.co.axiz.example.db.entity.base.BaseEntity;

public class Logintable implements BaseEntity {
	private int loginid;
	private String pass;
	public int getLoginid() {
		return loginid;
	}
	public void setLoginid(int loginid) {
		this.loginid = loginid;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
}
