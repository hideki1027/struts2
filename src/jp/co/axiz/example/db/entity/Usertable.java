package jp.co.axiz.example.db.entity;

import jp.co.axiz.example.db.entity.base.BaseEntity;

/**
 * Usertable��Entity
 * @author AxiZ
 *
 */
public class Usertable implements BaseEntity {
	private int userid;
	private String username;
	private String tel;

	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
}
