package jp.co.axiz.example.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;

@Results({
	@Result(name="success", location="/insertConfirm.jsp"),
	@Result(name="input", location="/insert.jsp")
})
public class InsertAction extends ActionSupport {
	private String username;
	private String tel;
	private String pass;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getPass() {
		return pass;
	}
	@RequiredStringValidator(key="errors.required", fieldName="PASS")
	@StringLengthFieldValidator(key="errors.length", fieldName="PASS", minLength="4", maxLength="10")
	public void setPass(String pass) {
		this.pass = pass;
	}

	@Action("insert")
	public String insertAction() throws Exception {
		// ��������
		return "success";
	}
}

