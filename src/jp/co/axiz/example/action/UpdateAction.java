package jp.co.axiz.example.action;

import java.util.ArrayList;

import jp.co.axiz.example.business.SelectBusiness;
import jp.co.axiz.example.db.entity.Logintable;
import jp.co.axiz.example.db.entity.Usertable;
import jp.co.axiz.example.db.entity.base.BaseEntity;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.RegexFieldValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;

@Results({
	@Result(name="success", location="/updateInput.jsp"),
	@Result(name="failure", location="/update.jsp"),
	@Result(name="input", location="/update.jsp")
})
public class UpdateAction extends ActionSupport {
	private String userid;
	private String username;
	private String tel;
	private String pass;
	public String getUserid() {
		return userid;
	}
	@RequiredStringValidator(key="errors.required", fieldName="ID")
	@RegexFieldValidator(key="errors.integer", fieldName="ID", regexExpression="[0-9_]*")
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}

	@Action("update")
	public String updateAction() {
		String strResult = "failure";
		try {
			// 入力されたIDで、ユーザ情報を検索
			ArrayList<BaseEntity> aryResult =
					new SelectBusiness().doSelectUserInfo(Integer.parseInt(this.getUserid()));

			// 結果の判定
			if(aryResult != null && aryResult.size() > 0) {
				Usertable usertable = (Usertable)aryResult.get(0);
				Logintable logintable = (Logintable)aryResult.get(1);

				// 値のセット
				this.setUsername(usertable.getUsername());
				this.setTel(usertable.getTel());
				this.setPass(logintable.getPass());

				// 正常に検索が完了した場合
				strResult  = "success";
			}
		} catch(Exception e) {
			System.out.println(e);
		}
		return strResult;
	}

}
