package jp.co.axiz.example.action;

import java.util.ArrayList;

import jp.co.axiz.example.business.SelectBusiness;
import jp.co.axiz.example.db.entity.Logintable;
import jp.co.axiz.example.db.entity.Usertable;
import jp.co.axiz.example.db.entity.base.BaseEntity;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;

@Results({
	@Result(name="success", location="/updateConfirm.jsp"),
	@Result(name="failure", location="/updateInput.jsp"),
	@Result(name="input", location="/updateInput.jsp")
})
public class UpdateInputAction extends ActionSupport {
	private String userid;
	private String username;
	private String tel;
	private String pass;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getPass() {
		return pass;
	}
	@RequiredStringValidator(key="errors.required", fieldName="PASS")
	@StringLengthFieldValidator(key="errors.length", fieldName="PASS", minLength="4", maxLength="10")
	public void setPass(String pass) {
		this.pass = pass;
	}

	@Action("updateInput")
	public String updateInputAction() {
		String strResult = "failure";
		try {
			// 入力されたIDで、ユーザ情報を検索
			ArrayList<BaseEntity> aryResult =
					new SelectBusiness().doSelectUserInfo(Integer.parseInt(this.getUserid()));

			// 結果の判定
			if(aryResult != null && aryResult.size() > 0) {
				Usertable usertable = (Usertable)aryResult.get(0);
				Logintable logintable = (Logintable)aryResult.get(1);

				// 値の変更があるか判定
				if(usertable.getUsername().equals(this.getUsername()) &&
						usertable.getTel().equals(this.getTel()) &&
						logintable.getPass().equals(this.getPass())) {
					// いずれの値も変更されていない
					this.setErrMsg("いずれの値も変更されていません");
				} else {
					// いずれかの値に変更があった場合
					strResult  = "success";
				}
			}
		} catch(Exception e) {
			System.out.println(e);
		}
		return strResult;
	}
	private String errMsg = "";
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
}
