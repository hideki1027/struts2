package jp.co.axiz.example.action;

import java.util.ArrayList;

import jp.co.axiz.example.business.DeleteBusiness;
import jp.co.axiz.example.db.entity.Logintable;
import jp.co.axiz.example.db.entity.Usertable;
import jp.co.axiz.example.db.entity.base.BaseEntity;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;

@Results({
	@Result(name="success", location="/deleteResult.jsp"),
	@Result(name="failure", location="/deleteConfirm.jsp"),
	@Result(name="input", location="/deleteConfirm.jsp")
})
public class DeleteConfirmAction extends ActionSupport {
	private String userid;
	private String username;
	private String tel;
	private String pass;
	private String passConfirm;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getPassConfirm() {
		return passConfirm;
	}
	@RequiredStringValidator(key="errors.required", fieldName="PASS(再)")
	@StringLengthFieldValidator(key="errors.length", fieldName="PASS(再)", minLength="4", maxLength="10")
	public void setPassConfirm(String passConfirm) {
		this.passConfirm = passConfirm;
	}

	@Action("deleteConfirm")
	public String updateConfirmAction() {
		String strResult = "failure";

		try {
			// 前の画面で入力されたPASSとPASS(再)が一致しているか判定
			if(this.getPassConfirm().equals(this.getPass())) {
				// 一致している場合、登録処理
				Usertable usertable = new Usertable();
				Logintable logintable = new Logintable();

				// 値をセット
				usertable.setUsername(this.getUsername());
				usertable.setTel(this.getTel());
				logintable.setPass(this.getPass());

				// 登録処理へのパラメータ定義
				ArrayList<BaseEntity> aryEntity = new ArrayList<BaseEntity>();
				aryEntity.add(usertable);
				aryEntity.add(logintable);

				// 登録処理の呼び出し
				new DeleteBusiness().doDeleteUserInfo(Integer.parseInt(this.getUserid()));

				// 正常に登録が完了した場合
				strResult  = "success";
			} else {
				// 一致していない場合、エラー
				this.setErrMsg("パスワードが一致していません");
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		// 処理結果
		return strResult;
	}
	private String errMsg = "";
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
}
