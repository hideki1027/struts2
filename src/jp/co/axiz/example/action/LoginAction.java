package jp.co.axiz.example.action;

import java.util.Map;

import jp.co.axiz.example.business.SelectBusiness;
import jp.co.axiz.example.db.entity.Usertable;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.RegexFieldValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;

@Results({
	@Result(name="success", location="/menu.jsp"),
	@Result(name="failure", location="/login.jsp"),
	@Result(name="input", location="/login.jsp")
})
public class LoginAction extends ActionSupport implements SessionAware {
	private String id;
	private String pass;
	public String getId() {
		return id;
	}
	@RequiredStringValidator(key="errors.required", fieldName="ID")
	@RegexFieldValidator(key="errors.integer", fieldName="ID", regexExpression="[0-9_]*")
	public void setId(String id) {
		this.id = id;
	}
	public String getPass() {
		return pass;
	}
	@RequiredStringValidator(key="errors.required", fieldName="PASS")
	@StringLengthFieldValidator(key="errors.length", fieldName="PASS", minLength="4", maxLength="10")
	public void setPass(String pass) {
		this.pass = pass;
	}

	private Map<String, Object> sessionMap;
	@Override
	public void setSession(Map<String, Object> session) {
		// TODO 自動生成されたメソッド・スタブ
		this.sessionMap = session;
	}

	@Action("login")
	private  String startOne() throws Exception {
		// 処理結果
		String strResult = "failure";
		// エラーメッセージに初期値
		this.setErrMsg("ログインに失敗しました");

		// 画面入力値でログイン処理を実行
		SelectBusiness business = new SelectBusiness();
		Usertable usertable = business.doLogin(Integer.parseInt(this.getId()), this.getPass());

		// 結果判定
		if (usertable != null) {
			// 結果がある場合は、ログイン成功
			strResult =  "success";

			// エラーメッセージを除去
			this.setErrMsg("");

			// セッションに値をセット
			this.sessionMap.put("USER_ID", usertable.getUserid());
			this.sessionMap.put("USER_NAME", usertable.getUsername());
		}

		// ログイン成功
		return strResult;
	}
	private String errMsg = "";
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

}
