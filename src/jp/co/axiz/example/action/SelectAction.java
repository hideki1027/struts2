package jp.co.axiz.example.action;

import java.util.ArrayList;

import jp.co.axiz.example.business.SelectBusiness;
import jp.co.axiz.example.db.entity.Usertable;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.RegexFieldValidator;

@Results({
	@Result(name="success", location="/selectResult.jsp"),
	@Result(name="failure", location="/select.jsp"),
	@Result(name="input", location="/select.jsp")
})
public class SelectAction extends ActionSupport {
	private String userid;
	private String userName;
	private String userTel;
	public String getUserid() {
		return userid;
	}
	@RegexFieldValidator(key="errors.integer", fieldName="ID", regexExpression="[0-9_]*")
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserTel() {
		return userTel;
	}
	public void setUserTel(String userTel) {
		this.userTel = userTel;
	}

	ArrayList<Usertable> aryUserInfo;
	public ArrayList<Usertable> getAryUserInfo() {
		return aryUserInfo;
	}
	public void setAryUserInfo(ArrayList<Usertable> aryUserInfo) {
		this.aryUserInfo = aryUserInfo;
	}
	@Action("select")
	public String selectAction() {
		String strResult = "failure";
		try {
			ArrayList<Object> aryParam = new ArrayList<>();

			// �����������Z�b�g
			aryParam.add(this.getUserid());
			aryParam.add(this.getUserName());
			aryParam.add(this.getUserTel());

			// �����̎��s
			this.setAryUserInfo(new SelectBusiness().doLogin(aryParam));

			// ����ɏI��
			strResult = "success";
		} catch(Exception e) {
			System.out.println(e);
		}

		return strResult;
	}
}
