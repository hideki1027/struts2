package jp.co.axiz.example.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;

@Results({
	@Result(name="success", location="/select.jsp")
})
public class ToSelectAction extends ActionSupport {
	private String userid;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	@Action("toSelect")
	public String toSelectAction() {
		return "success";
	}
}
