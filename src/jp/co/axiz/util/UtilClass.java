package jp.co.axiz.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Util�N���X
 * @author AxiZ
 *
 */
public class UtilClass {
	public static String getInputLine() throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		return reader.readLine();
	}
}
