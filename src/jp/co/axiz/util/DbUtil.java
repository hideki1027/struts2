package jp.co.axiz.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * データベースの接続・切断を行うクラス
 * @author AxiZ
 *
 */
public class DbUtil {
	// 定数定義
	private static final String DB_DRIVER = "org.postgresql.Driver";
	private static final String DB_NAME = "axizdb";
	private static final String DB_USER = "axizuser";
	private static final String DB_PASS = "axiz";
	private static final String DB_HOST = "127.0.0.1";
	private static final String DB_PORT = "5432";
	private static final String DB_URL = "jdbc:postgresql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME;

	// インスタンス化の禁止
	private DbUtil() {}

	/**
	 * DBのコネクションを取得
	 * @return DBコネクション
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName(DB_DRIVER);
		Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
		con.setAutoCommit(false);
		return con;
	}

	/**
	 * DBとのコネクションを切断
	 * @param con DBコネクション
	 * @throws SQLException
	 */
	public static void closeConnection(Connection con) throws SQLException {
		if (con != null) { con.close(); }
	}
}
