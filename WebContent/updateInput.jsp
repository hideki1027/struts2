<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel=stylesheet type="text/css" href="css/styleSheet.css">
		<title>UpdateInput</title>
	</head>
	<body>
		<div class="errMsg">
			<s:property value="errMsg" />
			<s:fielderror />
		</div>
		<span class="pageTitleMsg">データ変更</span>
		<br>変更したい項目を修正してください
		<br>※１項目以上変更してください
		<s:form action="/updateInput" theme="simple">
			<table border="1">
				<tr>
					<th>ID</th>
					<td>：</td>
					<td>
						<s:textfield readonly="true" cssClass="readonly" key="userid" />
					</td>
				</tr>
				<tr>
					<th>名前</th>
					<td>：</td>
					<td>
						<s:textfield key="username" />
					</td>
				</tr>
				<tr>
					<th>電話番号</th>
					<td>：</td>
					<td>
						<s:textfield key="tel" />
					</td>
				</tr>
				<tr>
					<th>PASS</th>
					<td>：</td>
					<td>
						<s:textfield key="pass" type="password"  />
					</td>
				</tr>
			</table>
			<s:submit value="確認" />
		</s:form>
	</body>
</html>