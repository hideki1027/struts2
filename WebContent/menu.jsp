<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel=stylesheet type="text/css" href="css/styleSheet.css">
		<title>Menu</title>
	</head>
	<body>
		<span class="pageTitleMsg"><s:property value="#session.USER_NAME" />さん、ようこそ！</span>
		<table class="tbStyle">
			<!-- 検索列 -->
			<tr>
				<td><a href="select.jsp">検索</a></td>
			</tr>

			<!-- 登録列 -->
			<tr>
				<td><a href="insert.jsp">登録</a></td>
			</tr>

			<!-- 更新列 -->
			<tr>
				<td><a href="update.jsp">更新</a></td>
			</tr>

			<!-- 削除列 -->
			<tr>
				<td><a href="delete.jsp">削除</a></td>
			</tr>
		</table>
	</body>
</html>