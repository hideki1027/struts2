<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel=stylesheet type="text/css" href="css/styleSheet.css">
		<title>DeleteConfirm</title>
	</head>
	<body>
		<div class="errMsg">
			<s:property value="errMsg" />
			<s:fielderror />
		</div>
		<span class="pageTitleMsg">削除確認</span>
		<s:form action="/deleteConfirm">
			<table>
				<tr>
					<th>ID</th>
					<td>：</td>
					<td>
						<s:textfield readonly="readonly" cssClass="readonly" key="userid" />
					</td>
				</tr>
				<tr>
					<th>名前</th>
					<td>：</td>
					<td>
						<s:textfield readonly="readonly" cssClass="readonly" key="username" />
					</td>
				</tr>
				<tr>
					<th>電話番号</th>
					<td>：</td>
					<td>
						<s:textfield readonly="readonly" cssClass="readonly" key="tel" />
					</td>
				</tr>
				<tr>
					<th>PASS(再)</th>
					<td>：</td>
					<td>
						<s:textfield key="passConfirm" type="password" />
						<s:textfield key="pass" type="hidden" />
					</td>
				</tr>
			</table>
			<s:submit value="削除" />
		</s:form>
	</body>
</html>