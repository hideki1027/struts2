<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel=stylesheet type="text/css" href="css/styleSheet.css">
		<title>SelectResult</title>
	</head>
	<body>
		<div class="errMsg">
			<s:property value="errMsg" />
			<s:fielderror />
		</div>
		<span class="pageTitleMsg">検索結果です。</span>
		<table>
			<thead>
				<tr>
					<td style="width: 20px;">ID</td>
					<td style="width: 40px;">名前</td>
					<td style="width: 20px;">電話番号</td>
				</tr>
			</thead>
			<tbody>
				<s:iterator value="aryUserInfo">
					<tr>
						<td><s:property value="userid" /></td>
						<td><s:property value="username" /></td>
						<td><s:property value="tel" /></td>
					</tr>
				</s:iterator>
			</tbody>
		</table>
		<s:form action="/toSelect" theme="simple">
			<s:submit property="submit" value="検索画面へ" />
		</s:form>
		<s:a href="menu.jsp" >メニューに戻る</s:a>
	</body>
</html>