<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel=stylesheet type="text/css" href="css/styleSheet.css">
		<title>Insert</title>
	</head>
	<body>
		<div class="errMsg">
			<s:property value="errMsg" />
			<s:fielderror />
		</div>
		<span class="pageTitleMsg">登録画面</span>
		<s:form action="/insert" theme="simple">
			<table border="1">
				<tr>
					<th>ID</th>
					<td>：</td>
					<td>
						<s:textfield readonly="true" cssClass="readonly" value="自動で設定されます" />
					</td>
				</tr>
				<tr>
					<th>名前</th>
					<td>：</td>
					<td>
						<s:textfield key="username" />
					</td>
				</tr>
				<tr>
					<th>電話番号</th>
					<td>：</td>
					<td>
						<s:textfield key="tel" />
					</td>
				</tr>
				<tr>
					<th>Pass</th>
					<td>：</td>
					<td>
						<s:password key="pass" />
					</td>
				</tr>
			</table>
			<s:submit value="確認" />
		</s:form>
	</body>
</html>