<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel=stylesheet type="text/css" href="css/styleSheet.css">
		<title>Update</title>
	</head>
	<body>
		<div class="errMsg">
			<s:property value="errMsg" />
			<s:fielderror />
		</div>
		<span class="pageTitleMsg">更新画面です。</span>
		<s:form action="/update" theme="simple">
			<table>
				<tr>
					<th>ID</th>
					<td>：</td>
					<td>
						<s:textfield key="userid" />
					</td>
				</tr>
			</table>
			<s:submit value="確認" />
		</s:form>
	</body>
</html>
<!----------------------------------------------------------------------------------------->