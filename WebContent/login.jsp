<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel=stylesheet type="text/css" href="css/styleSheet.css">
		<title>Login</title>
	</head>
	<body>
		<div class="errMsg">
			<s:property value="errMsg" />
			<s:fielderror />
		</div>
		<s:form action="/login" theme="simple">
			<table class="tbStyle">
				<tr>
					<td>ID</td>
					<td>：</td>
					<td><s:textfield key="id" /></td>
				</tr>
				<tr>
					<td>PASS</td>
					<td>：</td>
					<td><s:textfield key="pass" type="password" /></td>
				</tr>
				<tr>
					<td colspan="3">
						<s:submit value="ログイン" />
					</td>
				</tr>
			</table>
		</s:form>
	</body>
</html>